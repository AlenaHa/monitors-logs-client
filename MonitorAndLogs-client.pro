#-------------------------------------------------
#
# Project created by QtCreator 2017-01-03T19:51:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MonitorAndLogs-client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    proxycode.c

HEADERS  += mainwindow.h \
    proxycode.h

FORMS    += mainwindow.ui
