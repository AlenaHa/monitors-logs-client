#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include <QStandardItemModel>
#include <QTextStream>
#include <QByteArray>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Setting the names for the tabs.
    ui->tabWidget->setTabText(0, "Memory stats");
    ui->tabWidget->setTabText(1, "Process stats");
    ui->tabWidget->setTabText(2, "CPU stats");
    ui->tabWidget->setTabText(3, "User Stats");
    ui->tabWidget->setTabText(4, "Advanced");

    // Adjusting the size for the message Box.
    messageBox.setFixedSize(500, 300);

    // Creating the server's structure.

    struct sockaddr_in serverStruct;
    struct hostent *server;

    char *hostname = "0";
    int port = 8899;


    // Creating the socket.
    socketFd = socket(AF_INET, SOCK_STREAM, 0);
    if (socketFd < 0) {
        messageBox.critical(0, "Error", "Error opening socket!");
        exit(1);
    }

    // Get the server's DNS entry.
    server = gethostbyname(hostname);
    if (server == NULL) {
        messageBox.critical(0, "Error", "No such host!");
        exit(2);
    }

    // Creating the server's Internet address.
    bzero((char *) &serverStruct, sizeof(serverStruct));
    serverStruct.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
    (char *)&serverStruct.sin_addr.s_addr, server->h_length);
    serverStruct.sin_port = htons(port);

    // Connecting with the server.
    if (proxyconnect(socketFd, &serverStruct, sizeof(serverStruct)) < 0) {
        messageBox.critical(0, "Error", "Error connecting!");
        exit(3);
    }

    messageBox.information(0, "Info", "Successfully connected to server!");
}

MainWindow::~MainWindow()
{
    // Cosing the connetction.
    //  close (socketDes);

    delete ui;
}

/**
 * @brief MainWindow::csvResponseToModel This method converts a buffer into a CSV model.
 * @param buffer The buffer containing data from the db.
 * @return The model table.
 */
QStandardItemModel* MainWindow::csvResponseToModel(char* buffer) {

    QByteArray* responseBytes = new QByteArray(buffer);

    // Convert to table
    QStandardItemModel *model = new QStandardItemModel;

        // Buffer line counter.
        int lineindex = 0;
        // Read to text stream.
        QTextStream textStream(responseBytes, QIODevice::ReadOnly);

        while (!textStream.atEnd()) {

            // Read one line from textstream(separated by "\n")
            QString fileLine = textStream.readLine();

            // Parsing the read line into separate pieces with "," as the delimiter.
            QStringList lineToken = fileLine.split(",", QString::SkipEmptyParts);

            // Load parsed data to model.
            for (int j = 0; j < lineToken.size(); j++) {
                QString value = lineToken.at(j);
                QStandardItem *item = new QStandardItem(value);
                model->setItem(lineindex, j, item);
            }

            lineindex++;
        }

        return model;

}

/**
 * @brief MainWindow::interogateServer Asks server to interogate the db.
 * @param query The query from the client.
 * @return Server's response.
 */
char* MainWindow::interogateServer(QString query) {

    // Preparing the response message for the client.
    QByteArray byteArray = query.toLatin1();
    const char *byteArrayData = byteArray.data();

        // Get message from the user.
        bzero(buf, BUFSIZE);
        strcpy(buf, byteArrayData);
        printf(buf);

        // Build the query
        //strcpy(buf, query);

        // Send the message to the server.
        descriptor = write(socketFd, buf, strlen(buf));
        if (descriptor < 0) {
            messageBox.critical(0, "Error", "Error writing to server");
        }

        // Read server's reply.
        bzero(buf, BUFSIZE);
        descriptor = read(socketFd, buf, BUFSIZE);

        if (descriptor < 0) {
            messageBox.critical(0, "Error", "Error reading from server");
        }

        return buf;
}

/**
 * @brief MainWindow::runQueryAndUpdateTableWithResults
 * @param query The interogation at the server.
 * @return True if the table could be populated, false otherwise.
 */
bool MainWindow::runQueryAndUpdateTableWithResults(QString query)
{

        char* interogationResult;
        // Run the query (interogation at the server)
        interogationResult = interogateServer(query);

        // If an error occured, the table is no longer created, and an info box pops showing the error.
        if(strstr(interogationResult, "Error") == NULL) {
            // Transform the response into a QStandardItemModel
            QStandardItemModel* model = csvResponseToModel(buf);

            // Display the response in the QTableView
            ui->statistics_table->setModel(model);
        } else {
            messageBox.information(0, "Error",interogationResult);
            return false;
        }
        return true;
}

/**
 * @brief MainWindow::on_btn_advanced_query_clicked This method populates the table with data extracted from the db at the moment of click.
 */
void MainWindow::on_btn_advanced_query_clicked()
{
    // Getting the query from the user.
    QString query = ui->adv_query->toPlainText();

    // If the query might destroy the db, then it will not be executed, and instead an message box will appear.
    if(query.toLower().contains("drop") == false || query.toLower().contains("delete") == false) {
        runQueryAndUpdateTableWithResults(query);
    } else {
        messageBox.critical(0, "Attention", "The operation is forbidden.");
    }
}


void MainWindow::on_btn_proc_stats_clicked()
{
    // Creating the query.
    QString query = "SELECT * FROM process_stats limit 100";
    runQueryAndUpdateTableWithResults(query);

    // Changing columns names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Id"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("Active processes"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("PC user"));
    ui->statistics_table->model()->setHeaderData(3, Qt::Horizontal, QObject::tr("Timestamp"));

}

void MainWindow::on_proc_btn_per_days_clicked()
{
    // Create the query.
    QString query = "SELECT  sum(active_processes), pcuser, date(timestamp) from process_stats group by pcuser, date(timestamp)";
    runQueryAndUpdateTableWithResults(query);

    // Changing columns names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Active processes"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("PC user"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("Date"));

}

void MainWindow::on_proc_btn_per_hours_clicked()
{
    // Create the query.
    QString query = "SELECT sum(active_processes), pcuser, date(timestamp), date_part('hour', timestamp) FROM process_stats GROUP BY pcuser, date_part('hour',timestamp), date(timestamp)";
    runQueryAndUpdateTableWithResults(query);

    // Changing columns names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Active processes"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("PC user"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("Date"));
    ui->statistics_table->model()->setHeaderData(3, Qt::Horizontal, QObject::tr("Hour"));
}

void MainWindow::on_btn_mem_stats1_clicked()
{
    // Defining the query for this button.
    QString query = "SELECT * FROM mem_stats LIMIT 100";
    runQueryAndUpdateTableWithResults(query);

    // Change column names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Id"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("Total"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("Buffers"));
    ui->statistics_table->model()->setHeaderData(3, Qt::Horizontal, QObject::tr("Cached"));
    ui->statistics_table->model()->setHeaderData(4, Qt::Horizontal, QObject::tr("Free"));
    ui->statistics_table->model()->setHeaderData(5, Qt::Horizontal, QObject::tr("Shared"));
    ui->statistics_table->model()->setHeaderData(6, Qt::Horizontal, QObject::tr("Used"));
    ui->statistics_table->model()->setHeaderData(7, Qt::Horizontal, QObject::tr("Timestamp"));
}

void MainWindow::on_btn_per_day_clicked()
{
    // Defining the query for this button.
    // total, buffers, cached, free, shared, used, timestamp
    QString query = "SELECT avg(total), avg(buffers), avg(cached), avg(free), avg(shared), avg(used),  date(timestamp) from mem_stats group by date(timestamp)";
    runQueryAndUpdateTableWithResults(query);

    // Change column names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Total"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("Buffers"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("Cached"));
    ui->statistics_table->model()->setHeaderData(3, Qt::Horizontal, QObject::tr("Free"));
    ui->statistics_table->model()->setHeaderData(4, Qt::Horizontal, QObject::tr("Shared"));
    ui->statistics_table->model()->setHeaderData(5, Qt::Horizontal, QObject::tr("Used"));
    ui->statistics_table->model()->setHeaderData(6, Qt::Horizontal, QObject::tr("Date"));
}

void MainWindow::on_btn_per_hour_clicked()
{
    // Defining the query for this button.
    // total, buffers, cached, free, shared, used, timestamp
    QString query = "SELECT  avg(total), avg(buffers), avg(cached), avg(free), avg(shared), avg(used),  date(timestamp), date_part('hour', timestamp)  from mem_stats group by date(timestamp), date_part('hour', timestamp)";
    runQueryAndUpdateTableWithResults(query);

    // Change column names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Total"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("Buffers"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("Cached"));
    ui->statistics_table->model()->setHeaderData(3, Qt::Horizontal, QObject::tr("Free"));
    ui->statistics_table->model()->setHeaderData(4, Qt::Horizontal, QObject::tr("Shared"));
    ui->statistics_table->model()->setHeaderData(5, Qt::Horizontal, QObject::tr("Used"));
    ui->statistics_table->model()->setHeaderData(6, Qt::Horizontal, QObject::tr("Timestamp"));
    ui->statistics_table->model()->setHeaderData(7, Qt::Horizontal, QObject::tr("Hour"));
}

void MainWindow::on_btn_cpu_stats_clicked()
{
    // Creating the query.
    QString query = "SELECT * FROM cpu_stats limit 150";
    runQueryAndUpdateTableWithResults(query);

    // Setting the columns names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Id"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("Work Architecture"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("CPU op-mode(s)"));
    ui->statistics_table->model()->setHeaderData(3, Qt::Horizontal, QObject::tr("CPU op-mode(s)"));
    ui->statistics_table->model()->setHeaderData(4, Qt::Horizontal, QObject::tr("Byte Order"));
    ui->statistics_table->model()->setHeaderData(5, Qt::Horizontal, QObject::tr("CPU(s)"));
    ui->statistics_table->model()->setHeaderData(6, Qt::Horizontal, QObject::tr("On-line CPU(s) list"));
    ui->statistics_table->model()->setHeaderData(7, Qt::Horizontal, QObject::tr("Thread(s) per core"));
    ui->statistics_table->model()->setHeaderData(8, Qt::Horizontal, QObject::tr("Core(s) per socket"));
    ui->statistics_table->model()->setHeaderData(9, Qt::Horizontal, QObject::tr("Socket(s)"));
    ui->statistics_table->model()->setHeaderData(10, Qt::Horizontal, QObject::tr("NUMA node(s)"));
    ui->statistics_table->model()->setHeaderData(11, Qt::Horizontal, QObject::tr("Vendor ID"));
    ui->statistics_table->model()->setHeaderData(12, Qt::Horizontal, QObject::tr("CPU family"));
    ui->statistics_table->model()->setHeaderData(13, Qt::Horizontal, QObject::tr("Model"));
    ui->statistics_table->model()->setHeaderData(14, Qt::Horizontal, QObject::tr("Stepping"));
    ui->statistics_table->model()->setHeaderData(15, Qt::Horizontal, QObject::tr("CPU MHz"));
    ui->statistics_table->model()->setHeaderData(16, Qt::Horizontal, QObject::tr("Timestamp"));
}

void MainWindow::on_cpu_btn_per_day_clicked()
{
    // Creating the query.
    QString query = "SELECT avg(CPU_mhz), date(timestamp) FROM cpu_stats GROUP BY date(timestamp)";
    runQueryAndUpdateTableWithResults(query);

    // Setting the columns names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("CPU MHz"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("Date"));
}

void MainWindow::on_cpu_btn_per_hour_clicked()
{

    // Creating the query.
    QString query = "SELECT avg(CPU_mhz), date(timestamp), date_part('hour', timestamp) FROM cpu_stats GROUP BY date(timestamp), date_part('hour', timestamp)";
    runQueryAndUpdateTableWithResults(query);

    // Setting the columns names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("CPU MHz"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("Date"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("Hour"));

}

void MainWindow::on_btn_usr_stats_clicked()
{
    // Creating the query.
    QString query = "SELECT * FROM user_stats limit 1";
    runQueryAndUpdateTableWithResults(query);

    // Setting the column names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Id"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("User"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("TTY"));
    ui->statistics_table->model()->setHeaderData(3, Qt::Horizontal, QObject::tr("Login at"));
    ui->statistics_table->model()->setHeaderData(4, Qt::Horizontal, QObject::tr("Timestamp"));
    ui->statistics_table->model()->setHeaderData(5, Qt::Horizontal, QObject::tr("Comments"));
}

void MainWindow::on_usr_btn_per_day_clicked()
{
    // Creating the query.
    QString query = "SELECT count(date(login_at)) ,user_log, date(login_at) from user_stats GROUP BY user_log, date(login_at)";
    runQueryAndUpdateTableWithResults(query);

    // Setting the column names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Number of logins"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("User"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("Login at"));
}

void MainWindow::on_usr_btn_per_hour_clicked()
{
    // Creating the query.
    QString query = "SELECT count(date_part('hour', login_at)), user_log, date(login_at) ,date_part('hour', login_at) FROM user_stats GROUP BY user_log, date(login_at), date_part('hour', login_at)";
    runQueryAndUpdateTableWithResults(query);

    // Setting the column names.
    ui->statistics_table->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Number of logins"));
    ui->statistics_table->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("User"));
    ui->statistics_table->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("Login date"));
    ui->statistics_table->model()->setHeaderData(3, Qt::Horizontal, QObject::tr("Login hour"));

}
