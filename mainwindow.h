#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "proxycode.h"
#include <QMainWindow>
#include "QMessageBox"
#include <QStandardItemModel>
#include <QTextStream>
#include <QByteArray>

#define BUFSIZE 102400

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_btn_advanced_query_clicked();

    void on_btn_proc_stats_clicked();

    void on_proc_btn_per_days_clicked();

    void on_proc_btn_per_hours_clicked();

    void on_btn_mem_stats1_clicked();

    void on_btn_per_day_clicked();

    void on_btn_per_hour_clicked();

    void on_btn_cpu_stats_clicked();

    void on_cpu_btn_per_day_clicked();

    void on_cpu_btn_per_hour_clicked();

    void on_btn_usr_stats_clicked();

    void on_usr_btn_per_day_clicked();

    void on_usr_btn_per_hour_clicked();

private:
    Ui::MainWindow *ui;
    char buf[BUFSIZE];
    int socketFd, descriptor, socketDes;

    QMessageBox messageBox;
    QStandardItemModel* csvResponseToModel(char* buffer);
    char* interogateServer(QString query);
    bool runQueryAndUpdateTableWithResults(QString query);
};

#endif // MAINWINDOW_H
