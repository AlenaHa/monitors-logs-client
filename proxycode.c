#include "proxycode.h"
#include <sys/types.h>
#include <sys/socket.h>

int proxyconnect(int sockfd, struct sockaddr_in *addr,
                   socklen_t addrlen)
{
    return connect(sockfd, addr, addrlen);
}
