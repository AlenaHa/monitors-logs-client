#ifndef H_PROXYCODE
#define H_PROXYCODE

#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>

#ifdef __cplusplus
extern "C" {
#endif

int proxyconnect(int sockfd, struct sockaddr_in *addr,
                   socklen_t addrlen);

#ifdef __cplusplus
}
#endif

#endif /* H_PROXYCODE */
