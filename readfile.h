#ifndef READ_FILE
#define READ_FILE

#ifdef __cplusplus
extern "C"
{
#endif

   int readFile( FILE* fd, int a, int b, char *data );

#ifdef __cplusplus
};
#endif

#endif
